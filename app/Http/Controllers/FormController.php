<?php

namespace App\Http\Controllers;

use Mail;
use Curl;
use App\Otp;
use App\Mail\Lead;
use Illuminate\Http\Request;

class FormController extends Controller
{
   public function sendOtp($phone){
   	$code = rand(9999,999999);
   	Otp::create([
   		'phone' => $phone,
   		'code' => $code
   	]);

    $message = "Your OTP Code for verifying your Phone Number at SADGURU CLEANING is ".$code.", Thank you SADGURU CLEANING SERVICES";

    $message = str_replace(' ','%20',$message);

   	$url="http://way2promoadvertisers.in/shn/api/pushsms.php?usr=621097&key=010E0Of00PyFBOysOBsXD830HRjZU1&sndr=SDGURU&ph=".$phone."&text=".$message."&rpt=1";

	 $response = Curl::to($url)->get();
   if($response){
	   return json_encode('success');
   }
   return json_encode('error');
   }

   public function sendLead(Request $request){

    $code = OTP::where('code',$request->otp)->where('phone',$request->customer_phone)->first();

    if($code){
        $message  = "DEEP CLEANING LEAD, Customer ".$request->customer_name;
        $message .= ", Phone : ".$request->customer_phone;
      //  $message .= ", Email : ".(!is_null($request->customer_email)?$request->customer_email:'not specified');
        $message .= ", Service Date : ".(!is_null($request->date)?$request->date:'not specified');
      //  $message .= ", Service Time : ".(!is_null($request->time)?$request->time:'not specified');
        $message .= ", Service : ".(!is_null($request->service)?$request->service:'not specified');
     //   $message .= ", Service Type : ".(!is_null($request->service_type)?$request->service_type:'not specified');
      //  $message .= ", House Type : ".(!is_null($request->house_type)?$request->house_type:'not specified');
      //  $message .= ", Address : ".(!is_null($request->flat_no)?$request->flat_no:'not specified');
       // $message .= ", ".(!is_null($request->landmark)?$request->landmark:'not specified');
       // $message .= ", ".(!is_null($request->area)?$request->area:'not specified');

        $message = str_replace(" ","%20",$message);
        //9833163355
        $url="http://way2promoadvertisers.in/shn/api/pushsms.php?usr=621097&key=010E0Of00PyFBOysOBsXD830HRjZU1&sndr=SDGURU&ph=9833163355&text=".$message."&rpt=1";
        $response = Curl::to($url)->get();

        $message = "Thank You, We have received Your request. We will contact You Soon, SADGURU CLEANING SERVICES";
        $message = str_replace(' ','%20',$message);
        $url="http://way2promoadvertisers.in/shn/api/pushsms.php?usr=621097&key=010E0Of00PyFBOysOBsXD830HRjZU1&sndr=SDGURU&ph=".$request->customer_phone."&text=".$message."&rpt=1";
        $response = Curl::to($url)->get();

        $message  = "Customer : <strong>".$request->customer_name."</strong><br>";
        $message .= "Phone : <strong>".$request->customer_phone."</strong><br>";
        $message .= "Email : ".(!is_null($request->customer_email)?$request->customer_email:'not specified')."<br>";
        $message .= "Service Date: <strong>".(!is_null($request->date)?$request->date:'not specified')."</strong><br>";
        $message .= "Service Time : <strong>".(!is_null($request->time)?$request->time:'not specified')."</strong><br>";
        $message .= "Service : <strong>".(!is_null($request->service)?$request->service:'not specified')."</strong><br>";
        $message .= "Service Type : <strong>".(!is_null($request->service_type)?$request->service_type:'not specified')."</strong><br>";
        $message .= "House Type : <strong>".(!is_null($request->house_type)?$request->house_type:'not specified')."</strong><br>";
        $message .= "Shop / Flat No. : <strong>".(!is_null($request->shop_no)?$request->shop_no:'not specified')."</strong><br>";
        $message .= "Address : <strong>".(!is_null($request->landmark)?$request->landmark:'not specified')."</strong><br>";
        $message .= "Area : <strong>".(!is_null($request->area)?$request->area:'not specified')."</strong><br>";

        Mail::to('sales.sadgurufacility@gmail.com')->send(new Lead($message));
        Otp::where('phone',$request->customer_phone)->where('code',$request->otp)->delete();
        return json_encode('success');
    }else{
      return json_encode('error');
    }

   }
}

