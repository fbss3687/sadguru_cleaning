<div class="navstyle">
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><img src="{{ asset('images/logofull.png') }}" alt="Sadguru Logo" class="hidden-xs" id="logofull">
        </a>
        <a class="navbar-brand" href="/"><img src="{{ asset('images/logo1.png') }}" alt="Sadguru Logo" class="hidden hidden-xs" id="logo">
        </a>
        <a class="navbar-brand" href="/"><img src="{{ asset('images/logo1.png') }}" alt="Sadguru Logo" class="visible-xs" style="width: auto;height: 80px;">
        </a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
      
      <ul class="nav navbar-nav navbar-right">
       
        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <div style="border: 1px solid #1e1e1e;padding: 10px;">
            <i class="fa fa-cog fa-spin text-danger"></i> Services <span class="glyphicon glyphicon-chevron-down pull-right"></span>
            </div>
          </a>

          <ul class="dropdown-menu mega-dropdown-menu container">
         
            <li class="col-sm-4">
              <ul>
                <li class="dropdown-header">Home Deep Cleaning</li>
                <li><a href="javascript:;">Home Deep Cleaning</a></li>
                <li><a href="javascript:;">Deluxe Home Deep Cleaning</a></li>
                <li><a href="javascript:;">Kitchen Cleaning</a></li>
                <li><a href="javascript:;">Steam Kitchen Cleaning</a></li>
                <li><a href="javascript:;">Steam Bathroom Cleaning</a></li>
                <li><a href="javascript:;">Sofa Cleaning</a></li>
                <li><a href="javascript:;">Steam Sofa Cleaning</a></li>
                <li><a href="javascript:;">Mattress Cleaning</a></li>
                <li><a href="javascript:;">Steam Curtain Cleaning</a></li>
                <li><a href="javascript:;">Chair Cleaning</a></li>
                <li><a href="javascript:;">Carpet Cleaning</a></li>
                <li><a href="javascript:;">Sofa-cum Bed Cleaning</a></li>
                <li><a href="javascript:;">Steam Sofa-cum Bed Cleaning</a></li>
                
              </ul>
            </li>
            <li class="col-sm-4">
              <ul>
                <li class="dropdown-header">Office Deep Cleaning</li>
                <li><a href="javascript:;">Office Deep Cleaning</a></li>
                <li><a href="javascript:;">Chair Cleaning</a></li>
                <li><a href="javascript:;">Carpet Cleaning</a></li>
              </ul>
            </li>
            <li class="col-sm-4">
              <ul>
                <li class="dropdown-header">Pest Control Services</li>
                <li><a href="javascript:;">Cockroaches & Ant Control</a></li>
                <li><a href="javascript:;">Termite Control</a></li>
                <li><a href="javascript:;">Bed Bugs Control</a></li>
                <li><a href="javascript:;">Mosquito Control</a></li>
                <li><a href="javascript:;">Wood Borer Control</a></li>
                <li><a href="javascript:;">Rodent Control</a></li>
                <li><a href="javascript:;">Lizards Control</a></li>
                <li><a href="javascript:;">Bird Netting</a></li>
                <li><a href="javascript:;">Rat Guard Installation</a></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul> 

      <div class="navbar-right">
    
        <div class="right_content">
        <a href="mailto:sales.sadgurufacility@gmail.com">
        <i class="fa fa-envelope"></i> sales.sadgurufacility@gmail.com</a>
        <br/><i class="fa fa-phone"></i> 98 208 99502 / 98 331 63355</div>
      </div>

    </div>
    <!-- /.nav-collapse -->
    </div>
    <!--/.container-fluid -->
  </nav>
</div>