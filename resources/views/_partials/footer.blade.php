<div class="padding30px newsletter hidden-xs">
    <div class="container">
        <div class="col-sm-4">
            <h6>Subscribe to our Newsletter</h6>
        </div>
         <form>
            <div class="col-sm-6">
               <input type="email" name="email" placeholder="Enetr Your Email Id" required="">
            </div>
            <div class="col-sm-2">
                <button type="submit">Subscribe</button>
            </div>
        </form>
    </div>
</div>
<footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 myCols">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="javascript:;">Home</a></li>
                        <li><a href="javascript:;">Sign up</a></li>
                        <li><a href="javascript:;">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="javascript:;">Company Information</a></li>
                        <li><a href="javascript:;">Contact us</a></li>
                        <li><a href="javascript:;">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="javascript:;">FAQ</a></li>
                        <li><a href="javascript:;">Help desk</a></li>
                        <li><a href="javascript:;">Forums</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>Legal</h5>
                    <ul>
                        <li><a href="javascript:;">Terms of Service</a></li>
                        <li><a href="javascript:;">Terms of Use</a></li>
                        <li><a href="javascript:;">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="social-networks">
            <a href="https://www.facebook.com/mumbaipestcontrol/" target="_blank" class="facebook"><i class="fa fa-facebook-official"></i></a>
            <a href="https://twitter.com/pestcontrolspc" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="https://in.linkedin.com/in/sadgurupestcontrol" target="_blank" class="linkedin"><i class="fa fa-linkedin-square"></i></a>
            <a href="https://plus.google.com/105478517764906094312" target="_blank" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
            <p>© 2017 Sadguru Facility Services Pvt. Ltd. - All rights reserved | Designed &amp; Developed by: <a href="https://fourbrothers.co.in/" target="_blank">Four Bothers Software Solution</a></p>
        </div>
    </footer>

    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-danger">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Terms &amp; Conditions</h4>
        </div>
        <div class="modal-body">
            <div style="padding: 0 0 0 15px;">
                <ul type="circle">
                    <li>Cabinet and wardrobes cleaned by internal also if emptied by the customer in advance. Heavy furniture cannot be moved.</li>
                    <li>Unreachable and unsafe places will not be cleaned by our cleaner.</li>
                    <li>Customer has to arrange for stool/ladder, water supply and electrical connection.</li>
                    <li>Sofa, carpet and mattress foam/shampoo cleaning are available at additional charges.</li>
                    <li>For cancellation and reschedule, customer should inform us one day before of scheduled service day. Otherwise for cancellation 500/- and for rescheduling 250/- will be charged.</li>
                    <li>We shall not be held responsible if any valuables are lost & damages.</li>
                    <li>We don't give 100% guarantee on hard & old stains because more scrubbing will damage the fabric.</li>
                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                    <li>18% GST will be applicable on total service charges.</li>
                </ul>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>