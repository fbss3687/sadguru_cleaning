
  
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="csrf-token" content="{{csrf_token()}}">

    <meta name="theme-color" content="#4b4b4d">

  	<title>Sadguru Services</title>

  	<link rel="shortcut icon" href="{{ asset('images/logo1.png') }}">
  
  	<!--Bootstrap-3.3.7 CDN-->
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Animation Style-->
  <link rel="stylesheet" type="text/css" href=" {{ asset('css/animate.css') }} " />
  
  	<!-- Custom Style-->
  <link rel="stylesheet" type="text/css" href=" {{ asset('css/style.css') }} " />

  <link rel="stylesheet" type="text/css" href=" {{ asset('css/pikaday.css') }} " />

 	@yield('page-styles')

