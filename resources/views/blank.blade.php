@extends('layouts.master')

@section('page-styles')

@endsection

@section('body-content')
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-81406831-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-81406831-1');
	</script>
    <div class="container" style="margin-top: 15%;">
    	<center>
    		<i class="fa fa-check-circle-o fa-5x text-success" aria-hidden="true"></i>
    		<h1>Thank You !</h1>
    		<h4>We Will Contact You Soon.</h4>
    	</center>
    </div>
@endsection

@section('page-script')

@endsection
