<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

	<head>
		@include('_partials.head')
	</head>

	<body>
		@include('_partials.navbar')
		
		@yield('body-content')
		
		<!-- Return to Top -->
		<a href="javascript:;" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
		
		@include('_partials.footer')
		@include('_partials.scripts')
	</body>

</html>