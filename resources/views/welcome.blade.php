@extends('layouts.master')

@section('page-styles')

@endsection

@section('body-content')

    <img src="{{ asset('images/banner/deep_clean.jpeg') }}" alt="sadguru deep home cleaning" class="img-responsive hidden-xs">


        <div class="tab padding10px absolute_form">
        	<br class="visible-xs"/>
        	<br class="visible-xs"/>
        	<br class="visible-xs"/>
        	<br class="visible-xs"/>
            <h2 class="text-center">Cleaning Services</h2>
            <ul class="tabs">
                <li><a href="#">Book Cleaning Service</a></li>
                <li><a href="#">Home Cleaning Rate Card</a></li>
                <li><a href="#">Office Cleaning Rate Card</a></li>
                <li><a href="#">Frequently Asked Questions</a></li>
            </ul> <!-- / tabs -->

        <div class="tab_content" id="scroll">

            <div class="tabs_item">
               <!-- MultiStep Form -->
                <form class="form-horizontal" id="form" method="POST" action="">
                    {{csrf_field()}}
                    <div id="personel" class="">

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="name">Your Name:</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" placeholder="Enter Your Name" name="customer_name">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Email:</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" id="email" placeholder="Enter Email Address" name="customer_email">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="contact">Contact Number:</label>
                          <div class="col-sm-9" >
                            <input type="text" class="form-control" id="contact" placeholder="Enter 10 Digit Number" name="customer_phone">
                          </div>
                        </div>


                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="button" id="serviceBtn">Proceed </button>
                          </div>
                        </div>

                    </div>

                    <div id="services" class="animated slideInUp" style="display: none;">

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="datepicker">Service Date:</label>
                          <div class="col-sm-9" >
                            <input  class="form-control" type="text" id="datepicker" name="date"
                            value="{{date('M d,Y',strtotime(\Carbon\Carbon::now()))}}">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="timepicker">Service Time:</label>
                          <div class="col-sm-9" >
                            <select class="form-control" name="time" id="timepicker">
                                <option value=''  selected disabled >SELECT TIME</option>
                                <option value="9:00 AM to 10:00 AM">9:00 AM to 10:00 AM</option>
                                <option value="10:00 AM to 11:00 AM">10:00 AM to 11:00 AM</option>
                                <option value="11:00 AM to 12:00 AM">11:00 AM to 12:00 AM</option>
                                <option value="12:00 AM to 01:00 PM">12:00 AM to 01:00 PM</option>
                                <option value="01:00 PM to 02:00 PM">01:00 PM to 02:00 PM</option>
                                <option value="02:00 PM to 03:00 PM">02:00 PM to 03:00 PM</option>
                                <option value="03:00 PM to 04:00 PM">03:00 PM to 04:00 PM</option>
                                <option value="05:00 PM to 06:00 PM">05:00 PM to 06:00 PM</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="name">Service:</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="service" id="service">
                                <option value="" selected disabled> -- Select Service -- </option>
                                <option value="Home Deep Cleaning">Home Deep Cleaning</option>
                                <option value="Deluxe Home Deep Cleaning">Deluxe Home Deep Cleaning</option>
                                <option value="Kitchen Cleaning">Kitchen Cleaning</option>
                                <option value="Steam Kitchen Cleaning">Steam Kitchen Cleaning</option>
                                <option value="Steam Bathroom Cleaning">Steam Bathroom Cleaning</option>
                                <option value="Sofa Cleaning">Sofa Cleaning</option>
                                <option value="Steam Sofa Cleaning">Steam Sofa Cleaning</option>
                                <option value="Steam Mattress Cleaning">Steam Mattress Cleaning </option>
                                <option value="Steam Curtain Cleaning">Steam Curtain Cleaning</option>
                                <option value="Chair Cleaning">Chair Cleaning</option>
                                <option value="Steam Chair Cleaning">Steam Chair Cleaning</option>
                                <option value="Carpet Cleaning">Carpet Cleaning</option>
                                <option value="Steam Carpet Cleaning">Steam Carpet Cleaning</option>
                                <option value="Sofa-cum Bed Cleaning">Sofa-cum Bed Cleaning</option>
                                <option value="Steam Sofa-cum Bed Cleaning">Steam Sofa-cum Bed Cleaning</option>

                                <option value="Office Deep Cleaning">Office Deep Cleaning</option>
                                <option value="Office Chair Cleaning">Office Chair Cleaning</option>
                                <option value="Office Carpet Cleaning">Office Carpet Cleaning</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="contact">Service Type:</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="service_type" id="service_type">
                                <option value="" selected disabled>-- Select Service Type --</option>
                                <option value="One Time">One Time</option>
                                <option value="Half Yearly">Half Yearly</option>
                                <option value="Quarterly">Quarterly</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">House Type:</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="house_type" id="house_type" style="width:100%">
                                <option value="" selected disabled>-- Select Area --</option>
                                <option value="1 BHK">1 BHK ( upto 400sqft)</option>
                                <option value="2 BHK">2 BHK (401-800sqft)</option>
                                <option value="3 BHK">3 BHK (801-1200sqft)</option>
                                <option value="4 BHK">4 BHK (1201-1500sqft)</option>
                            </select>
                          </div>
                        </div>



                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="button" id="personelBtn" class="action-button-previous">Previous </button>
                            <button type="button" id="addBtn" class="">Proceed </button>
                          </div>
                        </div>

                    </div>

                    <div id="address" class="animated slideInUp" style="display: none;">

						        <div class="form-group">
                          <label class="control-label col-sm-3" for="shop">Shop / Flat No:</label>
                          <div class="col-sm-9" >
                            <input type="text" class="form-control" id="shop" placeholder="Shop /Flat No." required="" name="flat_no">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="landmark">Address:</label>
                          <div class="col-sm-9">
                          	<input type="text" class="form-control" id="landmark" placeholder="State / Landmark" name="landmark" required="">
                          </div>
                        </div>

						            <div class="form-group">
                          <label class="control-label col-sm-3" for="Area">Area / Pin Code:</label>
                          <div class="col-sm-9" >
                            <input type="text" class="form-control" id="Area" placeholder="Enter Landmark" required="" name="area">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="otp">OTP Number:</label>

                          <div class="col-sm-9">
                            <div class="input-group">
                              <input type="text" class="form-control" id="otp" placeholder="Enter OTP"  required="" name="otp">
                              <span>
                                <button type="button" class="input-group-addon bg-success" id="get_otp">
                                 <i class="fa fa-key"></i> GET OTP
                                </button>
                              </span>
                            </div>
                            <br>
                            <center class="text-danger" id="msg"></center>
                          </div>


                        </div>

                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="button" id="servicesBtn" class="action-button-previous">Previous </button>
                            <button type="submit" id="submitBtn">Submit </button>
                          </div>
                        </div>

                    </div>

                </form>

                <center>
                	<em>
                		<i class="fa fa-info-circle text-danger"></i> 18% GST will be applicable on total service charges.
                		<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-link text-danger">Terms &amp; Condtions</button>
                	</em>
                </center>

            </div> <!-- / tabs_item -->

			<div class="tabs_item">

				<div class="panel-group" id="rate_card" role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                    Home Deep Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>1 BHK</th>
                                        <th>2 BHK</th>
                                        <th>3 BHK</th>
                                        <th>4 BHK</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 3400</td>
                                        <td> <i class="fa fa-inr"></i> 4700</td>
                                        <td> <i class="fa fa-inr"></i> 5700</td>
                                        <td> <i class="fa fa-inr"></i> 6700</td>
                                    </tr>
                                    <!--<tr>
                                        <th>Half Yearly</th>
                                        <td> <i class="fa fa-inr"></i> 3400</td>
                                        <td> <i class="fa fa-inr"></i> 4700</td>
                                        <td> <i class="fa fa-inr"></i> 5700</td>
                                        <td> <i class="fa fa-inr"></i> 6700</td>
                                    </tr> -->
                                    <tr>
                                        <th>3 Service</th>
                                        <td> <i class="fa fa-inr"></i> 9800</td>
                                        <td> <i class="fa fa-inr"></i> 13200</td>
                                        <td> <i class="fa fa-inr"></i> 16050</td>
                                        <td> <i class="fa fa-inr"></i> 19000</td>
                                    </tr>
                                    <!--<tr>
                                        <th>Quarterly</th>
                                        <td> <i class="fa fa-inr"></i> 9800</td>
                                        <td> <i class="fa fa-inr"></i> 13200</td>
                                        <td> <i class="fa fa-inr"></i> 16050</td>
                                        <td> <i class="fa fa-inr"></i> 19000</td>
                                    </tr>-->
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Cleaning of bathroom tiles, shower, washbasin, taps, mirror, WC, cabinet and Disinfection of bathroom.</li>
                                        <li>Moping and cleaning of floor without moving heavy furniture.</li>
                                        <li>Wall cleaning to remove dry dusting and cobweb. (Not wet washing)</li>
                                        <li>Wiping of windows, window panel, dry vacuuming of window channels and dusting of grills.</li>
                                        <li>Dry vacuuming of sofa, chairs mattresses, carpet and curtains etc. (Not wet washing)</li>
                                        <li>Cleaning of lighting fixtures, door handles with wiping.</li>
                                        <li>Dusting of Gadgets and interiors.</li>
                                        <li>Cleaning of cabinets and wardrobes externally.</li>
                                        <li>Fridge, microwave, exhausted fans, stove and hob external cleaning with normal de-greasing of kitchen area.</li>
                                  </ol>

                                <h5>Service Hours: 4-5 Hours</h5>
                                <h5>No. Of Cleaners: 2-3 (Depend on Home Area)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>
                                    <li>Wet wiping for wall available with extra charges of Rs. 1400/- for 1 BHK, Rs. 1800/- for 2 BHK and Rs.2300/- for 3 BHK (Only washable painted walls)</li>
                                    <li>Cabinet and wardrobes cleaned by internal also if emptied by the customer in advance.</li>
                                    <li>Sofa, carpet and mattress foam/shampoo cleaning are available at additional charges.</li>
                                    <li>Customer has to arrange for stool/ladder, water supply and electrical connection.</li>
                                    <li>Unreachable and unsafe places will not be cleaned by our cleaner.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading2">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                   Deluxe Home Deep Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>1 BHK</th>
                                        <th>2 BHK</th>
                                        <th>3 BHK</th>
                                        <th>4 BHK</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 5800</td>
                                        <td> <i class="fa fa-inr"></i> 8000</td>
                                        <td> <i class="fa fa-inr"></i> 10800</td>
                                        <td> <i class="fa fa-inr"></i> 13600</td>
                                    </tr>
                                    <tr>
                                        <th>3 Service</th>
                                        <td> <i class="fa fa-inr"></i> 16500</td>
                                        <td> <i class="fa fa-inr"></i> 22800</td>
                                        <td> <i class="fa fa-inr"></i> 29800</td>
                                        <td> <i class="fa fa-inr"></i> 38800</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Steam cleaning is eco-friendly – no chemicals are involved. Steam cleaning is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites.</li>
                                        <li>Steam cleaning is excellent for those difficult-to- clean cracks and crevices.</li>
                                        <li>Cleaning of bathroom tiles, shower, washbasin, taps, mirror, WC, cabinet and Disinfection of bathroom.</li>
                                        <li>Moping and cleaning of floor without moving heavy furniture.</li>
                                        <li>Wall cleaning to remove dry dusting and cobweb with wiping if washable paint. (Wet washing)</li>
                                        <li>Wiping of windows, window panel, dry vacuuming of window channels and dusting of grills.</li>
                                        <li>Dry vacuuming of sofa, chairs mattresses, carpet and curtains etc. (Not wet washing)</li>
                                        <li>Cleaning of lighting fixtures, door handles with wiping.</li>
                                        <li>Dusting of Gadgets and interiors.</li>
                                        <li>Cleaning of cabinets and wardrobes externally.</li>
                                        <li>Fridge, microwave, exhausted fans, stove and hob external cleaning with steam de-greasing of kitchen area.</li>
                                  </ol>

                                <h5>Service Hours: 5-7 Hours</h5>
                                <h5>No. Of Cleaners: 3-5 (Depend on Home Area)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>
                                    <li>Cabinet and wardrobes cleaned by internal also if emptied by the customer in advance.</li>
                                    <li>Sofa, carpet and mattress foam/shampoo cleaning are available at additional charges.</li>
                                    <li>Customer has to arrange for stool/ladder, water supply and electrical connection.</li>
                                    <li>Unreachable and unsafe places will not be cleaned by our cleaner.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading3">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse3" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                    Kitchen Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>Regular</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 1300</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Wall cleaning to remove dry dusting and cobweb. (Not wet washing)</li>
                                        <li>Moping and cleaning of floor without moving heavy furniture.</li>
                                        <li>Wiping of windows, window panel, dry vacuuming of window channels and dusting of grills. (If reachable)</li>
                                        <li>Cleaning of Cupboard and shelves externally. (Internal if emptied by customer in advance)</li>
                                        <li>Fridge, microwave, exhausted fans, stove, Chimney and hob external cleaning with normal de-greasing of kitchen area.</li>
                                  </ol>

                                <h5>Service Hours: 2-4 Hours</h5>
                                <h5>No. Of Cleaners: 1-2 (Depend on Kitchen Area)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>
                                    <li>In this service we don’t provide utensil cleaning.</li>
                                    <li>Cleaning will be done only in kitchen area.</li>
                                    <li>Customer has to arrange for stool/ladder, water supply and electrical connection.</li>
                                    <li>Other appliances are not included in kitchen cleaning.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading4">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse4" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                   Steam Kitchen Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>

                                        <th>Steam</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>

                                        <td> <i class="fa fa-inr"></i> 2600</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Steam cleaning is eco-friendly – no chemicals are involved. Steam cleaning for kitchen is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites.</li>
                                        <li>Steam cleaning is excellent for those difficult-to- clean cracks and crevices.</li>
                                        <li>Steam is used for removing grease, oily surface and difficult stains.</li>
                                        <li>Wall cleaning to remove dry dusting and cobweb. (Wet washing)</li>
                                        <li>Moping and cleaning of floor without moving heavy furniture.</li>
                                        <li>Wiping of windows, window panel, dry vacuuming of window channels and dusting of grills. (If reachable)</li>
                                        <li>Cleaning of Cupboard and shelves externally. (Internal if emptied by customer in advance)</li>
                                        <li>Fridge, microwave, exhausted fans, stove, Chimney and hob external cleaning with steam de-greasing of kitchen area.</li>
                                  </ol>

                                <h5>Service Hours: 2-4 Hours</h5>
                                <h5>No. Of Cleaners: 1-2 (Depend on Kitchen Area)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>
                                    <li>In this service we don’t provide utensil cleaning.</li>
                                    <li>Cleaning will be done only in kitchen area.</li>
                                    <li>Customer has to arrange for stool/ladder, water supply and electrical connection.</li>
                                    <li>Other appliances are not included in kitchen cleaning.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading5">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse5" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                  Steam Bathroom Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 1400</td>
                                        <td> <i class="fa fa-inr"></i> 2000</td>
                                        <td> <i class="fa fa-inr"></i> 2800</td>
                                        <td> <i class="fa fa-inr"></i> 3400</td>
                                        <td> <i class="fa fa-inr"></i> 4200</td>
                                    </tr>

                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Steam cleaning is eco-friendly – no chemicals are involved. Steam cleaning for bathroom is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites.</li>
                                        <li>Steam cleaning is excellent for those difficult-to- clean cracks and crevices.</li>
                                        <li>Cleaning of bathroom tiles, shower, washbasin, taps, mirror, cabinet and Disinfection of bathroom with hand scrubbing.</li>
                                        <li>Moping and cleaning of the floor, washbasin, exhaust fan, window &amp; WC.</li>
                                  </ol>

                                <h5>Service Hours: 1-2 Hours</h5>
                                <h5>No. Of Cleaners: 1-2 (Depend on Bathroom Area)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>Cleaning will be done only in bathroom area.</li>
                                    <li>Customer has to arrange for stool/ladder, water supply and electrical connection.</li>

                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading6">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse6" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                   Sofa Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>3 Seats</th>
                                        <th>4 Seats</th>
                                        <th>5 Seats</th>
                                        <th>6 Seats</th>
                                        <th>7 Seats</th>
                                        <th>8 Seats</th>
                                        <th>9 Seats</th>
                                        <th>10 Seats</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 825</td>
                                        <td> <i class="fa fa-inr"></i> 1100</td>
                                        <td> <i class="fa fa-inr"></i> 1375</td>
                                        <td> <i class="fa fa-inr"></i> 1650</td>
                                        <td> <i class="fa fa-inr"></i> 1925</td>
                                        <td> <i class="fa fa-inr"></i> 2200</td>
                                        <td> <i class="fa fa-inr"></i> 2475</td>
                                        <td> <i class="fa fa-inr"></i> 2750</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Vacuum machine is used to remove dry dust, then with foam based shampooing (Fabric Sofa Only) &amp; scrubbing is done to remove stain &amp; Spots then special sucking vacuum machine is used, it will take 4 to 5 hrs. to get completely dry.</li>
                                  </ol>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-2 (Depend on no. of sofa seats)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>Cushion cleaning will be charged extra. (Small Cushion 50/- Each &amp; Big Cushion 100/- Each)</li>
                                    <li>Sofa should not be used until completely dry.</li>
                                    <li>We don&#39;t give 100% guarantee on hard &amp; old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water &amp; electrical connection.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading7">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse7" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                  Steam Sofa Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>3 </th>
                                        <th>4 </th>
                                        <th>5 </th>
                                        <th>6 </th>
                                        <th>7 </th>
                                        <th>8 </th>
                                        <th>9 </th>
                                        <th>10 </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 1500</td>
                                        <td> <i class="fa fa-inr"></i> 2000</td>
                                        <td> <i class="fa fa-inr"></i> 2500</td>
                                        <td> <i class="fa fa-inr"></i> 3000</td>
                                        <td> <i class="fa fa-inr"></i> 3500</td>
                                        <td> <i class="fa fa-inr"></i> 4000</td>
                                        <td> <i class="fa fa-inr"></i> 4500</td>
                                        <td> <i class="fa fa-inr"></i> 5000</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Steam cleaning is eco-friendly – no chemicals are involved.</li>
                                        <li>Steam cleaning for sofa is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites.</li>
                                        <li>Vacuum machine is used to remove dry dust, then with foam based shampooing (Fabric Sofa Only) &amp; steam scrubbing is done to remove stain &amp; Spots then  pecial sucking vacuum machine is used, it will take 4 to 5 hrs. to get completely dry.</li>
                                  </ol>

                                <h5>Service Hours: 1-4 Hours</h5>
                                <h5>No. Of Cleaners: 1-2 ((Depend on no. of sofa seats)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>
                                    <li>Cushion cleaning will be charged extra. (Small Cushion 75/- Each &amp; Big Cushion 125/- Each)</li>
                                    <li>Sofa should not be used until completely dry.</li>
                                    <li>We don&#39;t give 100% guarantee on hard &amp; old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water &amp; electrical connection.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading8">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse8" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                  Steam Mattress Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <caption>For Single Bed</caption>
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>1 Unit</th>
                                        <th>2 Unit</th>
                                        <th>3 Unit</th>
                                        <th>4 Unit</th>
                                        <th>5 Unit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 700  </td>
                                        <td> <i class="fa fa-inr"></i> 1400</td>
                                        <td> <i class="fa fa-inr"></i> 2100</td>
                                        <td> <i class="fa fa-inr"></i> 2800</td>
                                        <td> <i class="fa fa-inr"></i> 3500</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <caption>For Double Bed</caption>
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>1 Unit</th>
                                        <th>2 Unit</th>
                                        <th>3 Unit</th>
                                        <th>4 Unit</th>
                                        <th>5 Unit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 1400</td>
                                        <td> <i class="fa fa-inr"></i> 2800</td>
                                        <td> <i class="fa fa-inr"></i> 4200</td>
                                        <td> <i class="fa fa-inr"></i> 5600</td>
                                        <td> <i class="fa fa-inr"></i> 7000</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Steam cleaning for mattress is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites.</li>
                                        <li>The first cleaner used in the mattress cleaning process is to remove the dry dust by vacuum.</li>
                                        <li>After that, stain and spots are removed by shampooing &amp; steam scrubbing then special sucking vacuum machine is used.</li>
                                        <li>You can expect, it will take 4-6 hours to completely dry.</li>
                                  </ol>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-3 (Depend on no. of Mattress)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>Cushion cleaning will be charged extra. (Small Cushion 75/- Each &amp; Big Cushion 125/- Each)</li>
                                    <li>Mattress should not be used until completely dry.</li>
                                    <li>We don't give 100% guarantee on hard &amp; old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water &amp; electrical connection.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading9">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse9" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 Steam Curtain cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>1 </th>
                                        <th>2 </th>
                                        <th>3 </th>
                                        <th>4 </th>
                                        <th>5 </th>
                                        <th>6 </th>
                                        <th>7 </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 400</td>
                                        <td> <i class="fa fa-inr"></i> 800</td>
                                        <td> <i class="fa fa-inr"></i> 1200</td>
                                        <td> <i class="fa fa-inr"></i> 1600</td>
                                        <td> <i class="fa fa-inr"></i> 2000</td>
                                        <td> <i class="fa fa-inr"></i> 2400</td>
                                        <td> <i class="fa fa-inr"></i> 2800</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Steam cleaning for Curtain is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites.</li>
                                        <li>Vacuum machine is used to remove dry dust, then with foam based shampooing &amp; steam scrubbing is done to remove stain &amp; Spots then special sucking vacuum machine is used, it will take 4 to 5 hrs. to get completely dry.</li>

                                  </ol>

                                <h5>Service Hours: 1-2 Hours</h5>
                                <h5>No. Of Cleaners: 1-2 (Depend on no. of Curtains)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>Curtain should not be used until completely dry.</li>
                                    <li>Customer has to provide for water &amp; electrical connection.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading10">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse10" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 Chair cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>Upto 4</th>
                                        <th>5 </th>
                                        <th>6 </th>
                                        <th>7 </th>
                                        <th>8 </th>
                                        <th>9 </th>
                                        <th>10 </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 560</td>
                                        <td> <i class="fa fa-inr"></i> 700</td>
                                        <td> <i class="fa fa-inr"></i> 840</td>
                                        <td> <i class="fa fa-inr"></i> 980</td>
                                        <td> <i class="fa fa-inr"></i> 1120</td>
                                        <td> <i class="fa fa-inr"></i> 1260</td>
                                        <td> <i class="fa fa-inr"></i> 1400</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Dirt &amp; dust from the chair is removed by vacuum machine, then special foam based shampooing solution scrubbing/brushing done and excess water are extracted from the chair by vacuum, after extracting it will take 3 to 5 hrs to get completely dry.</li>

                                  </ol>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-3 (Depend on no. of Chairs))</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>We don't give 100% guarantee on hard &amp; old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water and electrical connection.</li>
                                    <li>Chairs should not be used until completely dry.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading14">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse14" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 Steam Chair Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14" >
                            <div class="panel-body" id="scroll">

                            	 <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>Upto 4</th>
                                        <th>5 </th>
                                        <th>6 </th>
                                        <th>7 </th>
                                        <th>8 </th>
                                        <th>9 </th>
                                        <th>10 </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 900</td>
                                        <td> <i class="fa fa-inr"></i> 1125</td>
                                        <td> <i class="fa fa-inr"></i> 1350</td>
                                        <td> <i class="fa fa-inr"></i> 1575</td>
                                        <td> <i class="fa fa-inr"></i> 1800</td>
                                        <td> <i class="fa fa-inr"></i> 2025</td>
                                        <td> <i class="fa fa-inr"></i> 2250</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>

                                  <h5>Service Details:</h5>
                                  <p>Steam cleaning for Chair is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites. Vacuum machine is used to remove dry dust, then with foam based shampooing (Fabric chair Only) & steam scrubbing is done to remove stain & Spots then special sucking vacuum machine is used, it will take 4 to 5 hrs. to get completely dry.</p>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-3 ( Depend on no. of Chairs )</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>We don't give 100% guarantee on hard & old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water and electrical connection.</li>
                                    <li>Chairs should not be used until completely dry.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading11">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse11" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 Carpet cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11" >
                            <div class="panel-body" id="scroll">
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>0-30 sqft</th>
                                        <th>30-50 sqft</th>
                                        <th>50-80 sqft</th>
                                        <th>80-150 sqft</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 800</td>
                                        <td> <i class="fa fa-inr"></i> 1000</td>
                                        <td> <i class="fa fa-inr"></i> 1400</td>
                                        <td> <i class="fa fa-inr"></i> 2700</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Every carpet cleaning starts with a proper method using a vacuum machine to remove dust &amp; soil.</li>
                                        <li>After removing dry soil and dust next step will start with foam based wet shampoo scrubbing to remove a stubborn stain.</li>
                                        <li>Once the shampooing process completed, water extraction process will start with powerfully vacuum machine for dry.</li>
                                        <li>It can take 3-5 hours for the carpet to dry after treatment.</li>

                                  </ol>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-3 (Depending on the carpet size)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>We don't give 100% guarantee on hard &amp; old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water and electrical connection.</li>
                                    <li>Chairs should not be used until completely dry.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading15">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse15" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                Steam Carpet Cleaning:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15" >
                            <div class="panel-body" id="scroll">

                            	<div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>0-30 sqft</th>
                                        <th>30-50 sqft</th>
                                        <th>50-80 sqft</th>
                                        <th>80-150 sqft</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 3000</td>
                                        <td> <i class="fa fa-inr"></i> 3500</td>
                                        <td> <i class="fa fa-inr"></i> 4000</td>
                                        <td> <i class="fa fa-inr"></i> 5200</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>

                                <h5>Service Details:</h5>
                                <ol>
                                    <li>Steam cleaning for Chair is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites.</li>
                                    <li>Every carpet cleaning starts with a proper method using a vacuum machine to remove dust & soil.</li>
                                    <li>After removing dry soil and dust next step will start with foam based wet shampoo & Steam scrubbing to remove a stubborn stain.</li>
                                    <li>Once the shampooing process completed, water extraction process will start with powerfully vacuum machine for dry.</li>
                                    <li>It can take 3-5 hours for the carpet to dry after treatment.</li>
                                </ul>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-3 (Depending on the carpet size)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>We don't give 100% guarantee on hard & old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water and electrical connection.</li>
                                    <li>Carpet should not be used until completely dry.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading12">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse12" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                  Sofa Cum Bed:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12" >
                            <div class="panel-body" id="scroll">
                            	<div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>One Time</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td> <i class="fa fa-inr"></i> 1700</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>

                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Vacuum machine is used to remove dry dust, then with foam based shampooing (Fabric Sofa cum bed Only) &amp;scrubbing is done to remove stain &amp; Spots then special sucking vacuum machine is used, it will take 4 to 5 hrs. to get completely dry.</li>

                                  </ol>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-2 (Depend on no. of sofa cum bed)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>Cushion cleaning will be charged extra. (Small Cushion 50/- Each &amp; Big Cushion 100/- Each)</li>
                                    <li>Sofa cum bed should not be used until completely dry.</li>
                                    <li>We don't give 100% guarantee on hard &amp; old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water &amp; electrical connection.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading13">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_card" href="#collapse13" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 Steam Sofa-cum- bed:
                                </a>
                            </h4>
                        </div>
                        <div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13" >
                            <div class="panel-body" id="scroll">

                            	<div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>One Time</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td> <i class="fa fa-inr"></i> 2100</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>

                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Steam cleaning is eco-friendly – no chemicals are involved.</li>
                                        <li>Steam cleaning for sofa cum bed is excellent because Steam cleaning kills 99.9 percent of bacteria, germs and dust mites.</li>
                                        <li>Vacuum machine is used to remove dry dust, then with foam based shampooing (Fabric Sofa cum bed Only) &amp; steam scrubbing is done to remove stain &amp; Spots then special sucking vacuum machine is used, it will take 4 to 5 hrs. to get completely dry.</li>
                                  </ol>

                                <h5>Service Hours: 1-4 Hours</h5>
                                <h5>No. Of Cleaners: 1-2 (Depend on no. of sofa cum bed)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>Cushion cleaning will be charged extra. (Small Cushion 75/- Each &amp; Big Cushion 125/- Each)</li>
                                    <li>Sofa cum bed should not be used until completely dry.</li>
                                    <li>We don't give 100% guarantee on hard &amp; old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water &amp; electrical connection.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>





				</div><!-- accordian group -->

			</div><!-- / tabs_item -->

           <div class="tabs_item">

				<div class="panel-group" id="rate_office_card" role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOfficeOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_office_card" href="#collapseOfficeOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 Office Deep Cleaning
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOfficeOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOfficeOne" >
                            <div class="panel-body" id="scroll">
								<div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>Upto 500 sqft</th>
                                        <th>500-800 sqft</th>
                                        <th>800-1000 sqft</th>
                                        <th>1001-2000 sqft</th>
                                        <th>More Than 2000</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 5000</td>
                                        <td> <i class="fa fa-inr"></i> 7200</td>
                                        <td> <i class="fa fa-inr"></i> 8000</td>
                                        <td> <i class="fa fa-inr"></i> 15000</td>
                                        <td> <i class="fa fa-inr"></i> Inspection</td>
                                    </tr>

                                    <tr>
                                        <th>Quarterly</th>
                                        <td> <i class="fa fa-inr"></i> 18400</td>
                                        <td> <i class="fa fa-inr"></i> 26400</td>
                                        <td> <i class="fa fa-inr"></i> 29600</td>
                                        <td> <i class="fa fa-inr"></i> 56000</td>
                                        <td>  Inspection</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                  <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Moping and cleaning the floor without moving heavy furniture.</li>
                                        <li>Wet washing (foam based Shampooing) by vacuuming of sofa, chairs and carpet.</li>
                                        <li>Wiping of windows, window panel, dry vacuuming of window channels and a dusting of grills.</li>
                                        <li>Dusting and wiping of desks, tables, desktops, computer and printer etc.</li>
                                        <li>Cleaning of lighting fixtures, doors and door handle with wiping.</li>
                                        <li>Cleaning of cabinets and wardrobes externally. (Internal if emptied by customer in advance)</li>
                                        <li>Fridge, microwave, exhausted fans, stove and coffee machine external cleaning with de-greasing of pantry area.</li>
                                        <li>Cleaning of washroom tiles, shower, washbasin, taps, mirror, WC, cabinet and Disinfection of washroom.</li>
                                        <li>Wall cleaning to remove dry dusting and cobweb. (Not wet washing)</li>
                                        <li>Moping and cleaning of conference rooms.</li>
                                  </ol>

                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>Cabinet and shelves cleaned by internal also if emptied by the customer in advance.</li>
                                    <li>Customer has to arrange for stool/ladder, water supply and electrical connection.</li>
                                    <li>Unreachable and unsafe places will not be cleaned by our cleaner.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOfficeTwo">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_office_card" href="#collapseOfficeTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 Office Chair Cleaning
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOfficeTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOfficeTwo" >
                            <div class="panel-body" id="scroll">
								<div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>5 - 10</th>
                                        <th>11 - 50</th>
                                        <th>51 - 100</th>
                                        <th>More Than 100</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 140 Per Chair</td>
                                        <td> <i class="fa fa-inr"></i> 110 Per Chair</td>
                                        <td> <i class="fa fa-inr"></i> 75 Per Chair</td>
                                        <td> <i class="fa fa-inr"></i> 60 Per Chair</td>
                                    </tr>

                                    <tr>
                                        <th>3 Service</th>
                                        <td> <i class="fa fa-inr"></i> 390 Per Chair</td>
                                        <td> <i class="fa fa-inr"></i> 300 Per Chair</td>
                                        <td> <i class="fa fa-inr"></i> 210 Per Chair</td>
                                        <td> <i class="fa fa-inr"></i> 150 Per Chair</td>
                                    </tr>
                                    </tbody>
                                </table>
                                 <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Dirt &amp; dust from the chair is removed by vacuum machine, then special foam based shampooing solution scrubbing/brushing done and excess water are extracted from the chair by vacuum, after extracting it will take 3 to 5 hrs to get completely dry.</li>

                                  </ol>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-3 (Depend on no. of Chairs))</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>We don't give 100% guarantee on hard &amp; old stains because more scrubbing will damage the fabric.</li>
                                    <li>Customer has to provide for water and electrical connection.</li>
                                    <li>Chairs should not be used until completely dry.</li>
                                    <li>Customer should be must verify the completed work, we do not do rework.</li>
                                </ol>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOfficeThree">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#rate_office_card" href="#collapseOfficeThree" aria-expanded="true" aria-controls="collapseThree">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 Office Carpet Cleaning
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOfficeThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOfficeThree" >
                            <div class="panel-body" id="scroll">
								<div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Duration</th>
                                        <th>Upto 500 sqft</th>
                                        <th>500-800 sqft</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>One Time</th>
                                        <td> <i class="fa fa-inr"></i> 9/sqft</td>
                                        <td> <i class="fa fa-inr"></i> 7/sqft</td>
                                    </tr>

                                    </tbody>
                                </table>
                                </div>

                                <h5>Service Details:</h5>
                                  <ol type="">
                                        <li>Every carpet cleaning starts with a proper method using a vacuum machine to remove dust &amp; soil.</li>
                                        <li>After removing dry soil and dust next step will start with foam based wet shampoo scrubbing to remove a stubborn stain.</li>
                                        <li>Once the shampooing process completed, water extraction process will start with powerfully vacuum machine for dry.</li>
                                        <li>It can take 3-5 hours for the carpet to dry after treatment.</li>

                                  </ol>

                                <h5>Service Hours: 1-3 Hours</h5>
                                <h5>No. Of Cleaners: 1-3 (Depending on the carpet size)</h5>
                                <h5 class="text-danger"><i class="fa fa-info-circle"></i> <em>Note:</em></h5>
                                <ol>

                                    <li>We don't give 100% guarantee on hard &amp; old stains because more

                            </div>
                        </div>
                    </div>

				</div>
			</div>

            <div class="tabs_item">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading14">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-minus"></i>
                                 When your Home needs a Deep Cleaning?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse14" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading14" >
                            <div class="panel-body" id="scroll">
                                You should go for Deep Cleaning if you have places or items in your house which are not covered in daily cleaning. Typically window sills & grooves, electrical fixtures, ceiling & exhaust fans accumulate dust as well as germs over time which are not visible to the naked eye. You might see yellowish stains on your bathroom floor & tiles and scaling on your taps and faucets or stubborn grease on your kitchen stove, platform & tiles as well. Deep cleaning takes care of these tough to clean areas of your home. What’s more, a steam wash will completely sanitize your entire house and give it a fresh looking shine.

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading15">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse15" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                 What do we do in Home Deep Cleaning?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15" >
                            <div class="panel-body" id="scroll">
                                We send a professional team of 3-4 champs with cleaning agents and equipments. They will first inspect your house, explain the process and the expected results to you. A complete Home Deep Cleaning takes 4-6 hours depending on the size of your house and covers kitchen, bathrooms, bedrooms, living areas & balcony including scrubbing of floor, tiles, kitchen platform etc, vacuuming of grooves, vents, window sills etc. and microfiber wiping of electrical fixtures, furnitures & artifacts, doors & windows and kitchen appliances. This is done with the help of branded cleaning agents & advanced equipment including steam machines.

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading16">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse16" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                What exactly is covered in minimum visiting charges?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse16" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16" >
                            <div class="panel-body" id="scroll">
                                Our visiting charges are not charges that you pay to us but to our vendor who has invested time, effort and commuting charges so as to fulfill your appointment. Therefore, if the order is not cancelled at least 2 hours in advance a nominal visiting fee will be levied.

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading17">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse17" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                               How long will the cleaning take?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17" >
                            <div class="panel-body" id="scroll">
                                Duration of the service depends on the size of the apartment. Generally our team takes around 4-6 hours to clean a 2 BHK apartment thoroughly.

                            </div>
                        </div>
                    </div>

                </div>
            </div> <!-- / tabs_item -->

        </div> <!-- / tab_content -->
        </div> <!-- / tab -->
    </div>


    <div class="container padding30px">
        <div class="row">
            <h3 class="text-center"><strong>About Sadguru Facility</strong></h3>
            <hr  align="center" width="50%" class="seprator" />
            <p class="text-center">
                We Sadguru Facility Services Pvt. Ltd are pleased to introduce ourselves as the professional cleaning services in Mumbai, New Mumbai & Thane. We are an ISO 9001:2015 (Quality), ISO 14001:2015 (Environmental) & OHSAS 18001:2007 (Health & Safety) Certified Company. Established in 2013, we are one of the fast growing company which sincerely provides quality services to clients all over Mumbai. No matter what type of cleaning is there in your house or office, whether it is sofa, bathroom, kitchen, chairs we can take care of anything with the help of our experienced and qualified team.
            </p>
        </div>
    </div>

    <div class="container padding30px">
        <div class="row">
            <h3 class="text-center"><strong>All Services</strong></h3>
            <hr  align="center" width="50%" class="seprator" />
            <div class="col-sm-2 col-sm-offset-2 col-xs-6">
            <div class="icon-div">
                <img src="{{ asset('images/icons/kichen_clean.jpeg') }}" class="img-responsive">
            </div>
            <h3 class="text-center">Kitchen Cleaning</h3>
        </div>
        <div class="col-sm-2 col-xs-6">
            <div class="icon-div">
                <img src="{{ asset('images/icons/steam_clean.jpeg') }}" class="img-responsive">
            </div>
            <h3 class="text-center">Steam Cleaning</h3>
        </div>
        <div class="visible-xs clearfix"></div>
        <div class="col-sm-2 col-xs-6">
            <div class="icon-div">
                <img src="{{ asset('images/icons/house_clean.jpeg') }}" class="img-responsive">
            </div>
            <h3 class="text-center">House Cleaning</h3>
        </div>
        <div class="col-sm-2 col-xs-6">
            <div class="icon-div">
                <img src="{{ asset('images/icons/washroom_clean.jpeg') }}" class="img-responsive">
            </div>
            <h3 class="text-center">Washroom Cleaning</h3>
        </div>
        </div>
        <div class="row">
         <div class="col-sm-2 col-sm-offset-3 col-xs-6">
            <div class="icon-div">
                <img src="{{ asset('images/icons/sofa.jpg') }}" class="img-responsive">
            </div>
            <h3 class="text-center">Sofa Cleaning</h3>
        </div>
        <div class="col-sm-2 col-xs-6">
            <div class="icon-div">
                <img src="{{ asset('images/icons/chair.jpg') }}" class="img-responsive">
            </div>
            <h3 class="text-center">Chair Cleaning</h3>
        </div>
        <div class="visible-xs clearfix"></div>
        <div class="col-sm-2 col-xs-6">
            <div class="icon-div">
                <img src="{{ asset('images/icons/mattress.jpg') }}" class="img-responsive">
            </div>
            <h3 class="text-center">Mattress Cleaning</h3>
        </div>
        </div>
    </div>

    <div class="bg-banner">
        <div class="container padding30px">
         <h3 class="text-center"><strong>Why Sadguru Facility?</strong></h3>
                   <hr  align="center" width="50%" class="seprator" />

            <div class="col-sm-2 col-sm-offset-1 col-xs-6">
                <div class="icon-div">
                    <img src="{{ asset('images/icons/executive.png') }}" class="img-responsive">
                </div>
                <h4 class="text-center">Trained Professional</h4>
            </div>
            <div class="col-sm-2 col-xs-6">
                <div class="icon-div">
                    <img src="{{ asset('images/icons/chemicals.png') }}" class="img-responsive">
                </div>
                <h4 class="text-center">WHO Approved Chemicals</h4>
            </div>
            <div class="col-sm-2 col-xs-6">
                <div class="icon-div">
                    <img src="{{ asset('images/icons/hidden_charges.png') }}" class="img-responsive">
                </div>
                <h4 class="text-center">No Hidden Charges</h4>
            </div>
            <div class="col-sm-2 col-xs-6">
                <div class="icon-div">
                    <img src="{{ asset('images/icons/technical_support.png') }}" class="img-responsive">
                </div>
                <h4 class="text-center">Reasonable Pricing SUPPORT</h4>
            </div>
            <div class="col-sm-2 col-xs-6">
                <div class="icon-div">
                    <img src="{{ asset('images/icons/rupee.png') }}" class="img-responsive">
                </div>
                <h4 class="text-center">Moneyback Guarantee</h4>
            </div>
        </div>
    </div>

    <div class="container padding30px">

            <div class="col-sm-6">
                <h3><strong>Testimonial</strong></h3>
                <div class="seprator"></div>
                    <div id="testimonial" class="carousel slide" data-ride="carousel">
                      <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                          <div class="row" style="padding: 20px">
                            <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para">I am impressed with the level of service I received from sadguru pest control. They were very professional, on time, knowledgeable and thorough.</p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="{{ asset('images/testimonial/Anubhav_Sinha.jpg') }}" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><strong>Anubhav Sinha</strong></h4>
                                <p class="testimonial_subtitle"><span> Bollywood Film Director</span>

                                </p>
                            </div>
                            </div>
                          </div>
                        </div>
                        <div class="item">
                           <div class="row" style="padding: 20px">
                            <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para">The mosquito treatment lasted well over a month. They arrived on time. Sprayed a larger area than the coupon stated. And, the treatment worked. I will definitely use them next year.</p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="{{ asset('images/testimonial/jason_dias.jpg') }}" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><strong>Jason Dias</strong></h4>
                                <p class="testimonial_subtitle"><span>-</span>
                                </p>
                            </div>
                            </div>
                          </div>
                        </div>
                        <div class="item">
                           <div class="row" style="padding: 20px">
                            <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para">These guys are extremely cordial and professional. Guarantee their work and are always on time with a smile. I would recommend them any time.</p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="{{ asset('images/testimonial/vikram.jpg') }}" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><strong>Vikram Singh</strong></h4>
                                <p class="testimonial_subtitle"><span>-</span>
                                </p>
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="controls testimonial_control pull-right">
                        <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#testimonial" data-slide="prev"></a>

                        <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#testimonial" data-slide="next"></a>
                    </div>
            </div>

            <div class="col-sm-6">
                 <iframe width="100%" height="315" src="https://www.youtube.com/embed/nbmJnCHXaW8" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
            </div>

    </div>




@endsection

@section('page-script')
   <script>



   if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

	}
	else
	{

		   $(window).scroll(function() {
		    var scroll = $(window).scrollTop();

		    if (scroll >= 100) {
		        // $('nav').addClass('navbar-default');
		        $('#logofull').addClass('hidden');
		        $('#logo').removeClass('hidden');
		        $('nav').css('height','60px');
		        $('.navstyle .nav >li >a ').css('margin-top','-20px');
		        $('.navstyle .navbar-brand').css({'height':'125px','margin-top':'-15px'});
		    } else {
		        // $('nav').removeClass('navbar-default');
		        $('#logofull').removeClass('hidden');
		        $('#logo').addClass('hidden');
		        $('nav').css('height','');
		        $('.navstyle .nav >li >a ').css('margin-top','0px');
		        $('.navstyle .navbar-brand').css({'height':'','margin-top':''});
		    }
		});

	}

    $('#serviceBtn').click(function(event) {
        if($("[name='customer_name']").val()==""){
            $("[name='customer_name']").addClass('input-required');
            return;
        }else{
            $("[name='customer_name']").removeClass('input-required');
        }

        if($("[name='customer_phone").val()!=""){
            let pattern = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
            if(!pattern.test($("[name='customer_phone']").val())){
                $("[name='customer_phone']").addClass('input-required');
                return;
            }else{
                $("[name='customer_phone']").removeClass('input-required');
            }
        }else{

            $("[name='customer_phone']").addClass('input-required');
            return;
        }

        $('#services').show();
        $('#personel').hide();


    });
     $('#personelBtn').click(function(event) {

        $('#services').hide();
        $('#personel').show();
        $('#personel').addClass('animated slideInUp');

    });

     $('#addBtn').click(function(event) {

        if($("#datepicker").val()==''){
            $("#datepicker").addClass('input-required');
            return;
        }else{
             $("#datepicker").removeClass('input-required');
        }

        if($("#timepicker").val()==null){
            $("#timepicker").addClass('input-required');
            return;
        }else{
             $("#timepicker").removeClass('input-required');
        }

        if($("[name='service']").val()==null){
            $("[name='service']").addClass('input-required');
            return;
        }else{
            $("[name='service']").removeClass('input-required');
        }

        if($("[name='service_type']").val()==null){
            $("[name='service_type']").addClass('input-required');
            return;
        }else{
            $("[name='service_type']").removeClass('input-required');
        }

        if($("[name='house_type']").val()==null){
             $("[name='house_type']").addClass('input-required');
            return;
        }else{
             $("[name='house_type']").removeClass('input-required');
        }

        $('#address').show();
        $('#personel').hide();
        $('#services').hide();

    });

    $('#submitBtn').click(function(event){
        event.preventDefault();

        if($("#shop").val()==""){
            $("#shop").addClass('input-required');
            return;
        }else{
             $("#shop").removeClass('input-required');
        }

        if($("#landmark").val()==""){
            $("#landmark").addClass('input-required');
            return;
        }else{
             $("#landmark").removeClass('input-required');
        }

        if($("#area").val()==""){
            $("#area").addClass('input-required');
            return;
        }else{
             $("#area").removeClass('input-required');
        }

        if($("#otp").val()==""){
            $("#otp").addClass('input-required');
            return;
        }else{
             $("#otp").removeClass('input-required');
        }
        $('#msg').html('');
        $.ajax({
            url: '/lead_form',
            method: 'POST',
            data: $('#form').serialize(),
        }).done(function(response) {
          console.log(response);
            if(JSON.parse(response)=='success'){
              window.location = '/thanks';
            }else{
              $('#msg').html('INVALID CODE, TRY AGAIN');
            }
            return false;
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log('ajax call failed')
        }).always(function() {
        });


        return false;
    });

    $('#servicesBtn').click(function(event) {
        $('#services').show();
        $('#personel').hide();
        $('#address').hide();
    });

    $('#get_otp').click(function(){
      let phone = $("[name='customer_phone").val();
      $('#msg').html('');
      $.ajax({
          url: '/sendOtp/'+phone,
        }).done(function(response) {

          if(JSON.parse(response)=='success'){
            $('#msg').html('OTP SENT TO YOUR PHONE');
          }else{
            $('#msg').html('OTP SENT TO YOUR PHONE')
          }

        }).fail(function(jqXHR, textStatus, errorThrown) {
          console.log('ajax call failed')
        }).always(function() {
      });
    });


</script>

@endsection
